# Badges

[![Netlify Status](https://api.netlify.com/api/v1/badges/3b5e0901-a0ef-4be3-a350-077b37489c25/deploy-status)](https://app.netlify.com/sites/nuxtbase/deploys)

# codesandbox-nuxt

> Nuxt starter for CodeSandBox (used for https://template.nuxtjs.org)

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn run dev

# build for production and launch server
$ yarn run build
$ yarn start

# generate static project
$ yarn run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
